
# Supremacy #

Supremacy is a non-commercial fan-made "4X" (eXplore, eXpand, eXploit, eXterminate) turn-based, empire-building strategy game on the galactic level.
The game is intended to be a spiritual successor to "Birth of the Federation", which was developed in the late 1990s by now-defunct MicroProse.
Although Mike Strobel, the creator of Supremacy, is no longer developing the game, this project is commited to continuing his work.

The latest build is available in the Downloads area. There's no installer yet, just unzip the file 

Feedback is much appreciated, the discussion forum is located [here](https://www.armadafleetcommand.com/onscreen/botf/viewforum.php?f=300)


### Pre-Requisites ###

- Desktop resolution of at least 1024x768 (1280x768 or higher is strongly recommended)

- Microsoft .NET Framework 4.5.1+

- Microsoft XNA Framework 3.1 Redistributable

- DirectX 9 or higher

- DirectX 9-compatible video card with Pixel Shader 2.0+ and Vertex Shader 1.1+ support

- Microsoft DirectX Runtime